
# coding: utf-8

# # Water supply at a municipal level (downscaled)
# 
# ## SIDRA API

# **UN Environment**

# In[3]:

import datetime; print(datetime.datetime.now())


# In[1]:

import pandas as pd
import numpy as np
import statsmodels.api as sm
from scipy import stats
import matplotlib.pylab as plt
import seaborn as sns
sns.set(style="darkgrid", color_codes=True)


# In[8]:

downscaled_data = pd.read_csv('downscaled_data_cat.csv', index_col=0)


# In[9]:

downscaled_data.dtypes


# In[7]:

downscaled_data.columns


# In[21]:

formula = "water ~ C(ban) + C(dutyp) + C(income) + C(connection) + C(urban) + C(sex) + C(age)"


# In[22]:

#result_cap = result.div(capita, axis=0).mul(1000)
#result_cap = result_cap.dropna()

model_cap = sm.OLS.from_formula(formula, downscaled_data)


# In[25]:

model_results_cap = model_cap.fit()
print(model_results_cap.summary())


# In[ ]:




# In[ ]:



