
# coding: utf-8

# In[151]:

from API.sidra import getTable
import numpy as np
import pandas as pd


# In[152]:

indicators=['water', 'income', 'urban', 'connection', 'ban', 'dutyp', 'sex', 'age']
result, groups = getTable(cap=15000, indicators=indicators, specific=False)


# In[155]:

from numpy.random import choice
def update_dummy(x, seq):
    sel = choice(seq, p=x)
    cat = result.columns[sel]
    return(cat)


# In[156]:

result_w = result.copy()
for g, seq in groups.items():
    result_w.iloc[:, seq] = result.iloc[:, seq].div(result.iloc[:, seq].sum(axis=1), axis=0)


# In[165]:

do_list = list()
for row_index in range(result.shape[0]):
    print("{}/{}".format(row_index, result_w.shape[0]), end='\r')
    cat_m = list()
    for _ in range(10):
        cat_mi = list()
        for g, seq in groups.items():
            if g == 'water':
                capita = result.iloc[row_index, groups['urban']].sum()
                val = result.iloc[row_index, seq].div(capita).mul(1000)
                cat = val.iloc[0]
            else:
                cat = update_dummy(result_w.iloc[row_index, seq], seq)
            cat_mi.append(cat)
        cat_m.append(cat_mi)
    cat_o = pd.DataFrame(cat_m, columns=groups.keys()).drop_duplicates()
    do_list.append(cat_o)
do = pd.concat(do_list, ignore_index=True, axis=0)
do.to_csv('downscaled_data_cat.csv')

