
Statistical analysis of supplied water at a municipal level
===========================================================

(a) SIDRA API
-------------

**UN Environment**

.. code:: ipython3

    import datetime; print(datetime.datetime.now())


.. parsed-literal::

    2017-08-30 16:32:34.706575


The following document presents a multiple regression analysis of
supplied water consumption at a municipal level for the country of
Brazil. We use available water consumption data at a municipal level to
estimate the distribution of water consumption at a household level. We
make use of the SIDRA-API for the data collection. In order to get the
data for all municipalities we download subsamples of data iteratively.

Import local libraries.
-----------------------

The ``parseData`` script downloads and prepares municipal level data
retrieved trough the SIDRA-API.

.. code:: ipython3

    from API.sidra import getTable


.. parsed-literal::

    /usr/lib/python3.6/site-packages/statsmodels/compat/pandas.py:56: FutureWarning: The pandas.core.datetools module is deprecated and will be removed in a future version. Please use the pandas.tseries module instead.
      from pandas.core import datetools


We make use of standard python libraries for the statistical analysis
and plotting of results.

.. code:: ipython3

    import pandas as pd
    import numpy as np
    import statsmodels.api as sm
    from scipy import stats
    import matplotlib.pylab as plt
    import seaborn as sns
    sns.set(style="darkgrid", color_codes=True)

The ``cap`` parameter sets the value of the upper bound for the water
supply variable. The model under performs with large values and therefor
we set the upper bound to 15000.

.. code:: ipython3

    indicators=['water', 'income', 'urban', 'connection', 'ban', 'dutyp']
    result = getTable(cap=15000, indicators=indicators, specific=False)


.. parsed-literal::

    /home/esteban/Documents/UNEP/Data/Brasil-SIDRA-API/API/DATA/
    DU typ	= 44795101	Households	Tab: 1442
    HH size	= 3.62*
    Income	= 44795101	Households	Tab: 3177
    HH size	= 4.26*
    Urban	= 44795101	Households	Tab: 761
    Conn	= 44795101	Households	Tab: 1442
    Toilets	= 44795101	Households	Tab: 1450
    Water	= 56739726	m^3		Tab: 1773
    * estimated values


.. code:: ipython3

    print(result.head())


.. parsed-literal::

             Volume total de água com tratamento  Não tinham  1-5 banheiros  \
    D4C                                                                       
    1200054                                750.0       665.0          120.0   
    1200104                               5000.0      3020.0         1049.0   
    1200138                               1794.0      1114.0          241.0   
    1200179                                900.0      1138.0          120.0   
    1200203                               7800.0     10546.0         3395.0   
    
             Cômodo  Apartamento, Casa  Sem rendimento  Até 1 de salário mínimo  \
    D4C                                                                           
    1200054     9.0              776.0      674.519008               334.908782   
    1200104    47.0             4022.0     2078.315024              1696.115241   
    1200138    12.0             1343.0      842.111676               633.035674   
    1200179     4.0             1254.0      857.598788               659.585007   
    1200203    87.0            13854.0     7948.759868              5249.024504   
    
             Mais de 1 a 5 salários mínimos  Mais de 5 a 15 salários mínimos  \
    D4C                                                                        
    1200054                      236.178447                        19.358889   
    1200104                      837.686787                        90.710223   
    1200138                      328.548004                        26.549334   
    1200179                      337.397782                        27.932111   
    1200203                     3085.253813                       382.199782   
    
             Mais de 15 salários mínimos  Rede geral    Outra        Rural  \
    D4C                                                                      
    1200054                     1.106222       362.0    423.0   557.070194   
    1200104                    10.785667       968.0   3101.0  1677.081897   
    1200138                     4.701444       322.0   1033.0  1122.125375   
    1200179                     3.042111        58.0   1200.0  1143.496955   
    1200203                    56.417334      3937.0  10004.0  5444.116433   
    
                   Urbana  
    D4C                    
    1200054    868.954351  
    1200104   3348.292480  
    1200138    867.310383  
    1200179    922.735579  
    1200203  12993.450919  


**Table 1: Data structure**

In addition to the upper bound set by ``cap`` we limit the sample to
plausible per-capita values. We define these values between 1 and 500.

.. code:: ipython3

    capita = result.iloc[:,-4:-2].sum(axis=1)
    result.loc[:, 'watercp'] = result.iloc[:,0].div(capita).mul(1000)
    result = result[(result.watercp <= 500) & (result.watercp >= 1)]

.. code:: ipython3

    g = sns.distplot(result.iloc[:,-1], kde=False, fit=stats.gamma)
    g.set_xlabel(u'Per-capita daily water demand [l/day/capita]')
    plt.text(610, 0.0001, 'Data source: IBGE SIDRA-API Table 1773', ha='right', va='center');
    plt.savefig("FIGURES/water_demand.png", dpi=300)



.. image:: SIDRA-API_files/SIDRA-API_14_0.png


**Figure 1: Estimated per-capita water consumption at a municipal
level**

We fit the statistical model with an ``OLS`` regression.

.. code:: ipython3

    X = result.ix[:,1:-1]
    y = result.ix[:,0]
    
    model = sm.OLS(y, X)
    model_results = model.fit()
    print(model_results.summary())


.. parsed-literal::

                                         OLS Regression Results                                    
    ===============================================================================================
    Dep. Variable:     Volume total de água com tratamento   R-squared:                       0.873
    Model:                                             OLS   Adj. R-squared:                  0.872
    Method:                                  Least Squares   F-statistic:                     1373.
    Date:                                 Wed, 30 Aug 2017   Prob (F-statistic):               0.00
    Time:                                         16:26:31   Log-Likelihood:                -17188.
    No. Observations:                                 2218   AIC:                         3.440e+04
    Df Residuals:                                     2207   BIC:                         3.446e+04
    Df Model:                                           11                                         
    Covariance Type:                             nonrobust                                         
    ===================================================================================================
                                          coef    std err          t      P>|t|      [0.025      0.975]
    ---------------------------------------------------------------------------------------------------
    Não tinham                         -0.1789      0.067     -2.652      0.008      -0.311      -0.047
    1-5 banheiros                      -0.0005      0.063     -0.008      0.994      -0.124       0.123
    Cômodo                             -0.5970      0.253     -2.363      0.018      -1.092      -0.102
    Apartamento, Casa                   0.4176      0.125      3.340      0.001       0.172       0.663
    Sem rendimento                     -0.2934      0.149     -1.974      0.049      -0.585      -0.002
    Até 1 de salário mínimo             0.3557      0.112      3.163      0.002       0.135       0.576
    Mais de 1 a 5 salários mínimos     -0.1009      0.131     -0.773      0.440      -0.357       0.155
    Mais de 5 a 15 salários mínimos     0.4676      0.381      1.226      0.220      -0.281       1.216
    Mais de 15 salários mínimos       -16.0860      2.071     -7.767      0.000     -20.148     -12.024
    Rede geral                         -0.0729      0.067     -1.093      0.275      -0.204       0.058
    Outra                              -0.1065      0.063     -1.702      0.089      -0.229       0.016
    Rural                               0.0690      0.116      0.597      0.551      -0.158       0.296
    Urbana                              0.0987      0.117      0.843      0.399      -0.131       0.328
    ==============================================================================
    Omnibus:                      907.834   Durbin-Watson:                   2.030
    Prob(Omnibus):                  0.000   Jarque-Bera (JB):            42638.955
    Skew:                          -1.179   Prob(JB):                         0.00
    Kurtosis:                      24.350   Cond. No.                     1.62e+16
    ==============================================================================
    
    Warnings:
    [1] Standard Errors assume that the covariance matrix of the errors is correctly specified.
    [2] The smallest eigenvalue is 6.19e-22. This might indicate that there are
    strong multicollinearity problems or that the design matrix is singular.


The model is statistical significant.

.. code:: ipython3

    result['Estimated treated water'] = result.apply(lambda row: model_results.predict(row[1:-1])[0], axis=1)

.. code:: ipython3

    g = sns.jointplot(x='Estimated treated water',
                      y='Volume total de água com tratamento',
                      data=result, kind='reg')
    g.set_axis_labels('Estimated treated water', 'Observed treated water')
    plt.text(7000, 0,
    """Data source:
    IBGE SIDRA-API Tab.:
        1442, 3177, 761,
        1442, 1450, 1773""",
             ha='left', va='center');
    plt.tight_layout()
    plt.savefig('FIGURES/estimated_water.png', dpi=300)



.. image:: SIDRA-API_files/SIDRA-API_20_0.png


**Figure 2: Performance of the model**
